// @ts-check

import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { render, screen, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';

import reducers from '../src/reducers/index.js';
import App from '../src/components/App.jsx';

test('Store', () => {
  const store = createStore(reducers);

  const vdom = (
    <Provider store={store}>
      <App />
    </Provider>
  );
  render(vdom);

  expect(screen.getByRole('textbox')).toBeInTheDocument();
  expect(screen.getByRole('button', { name: /add/i })).toBeInTheDocument();
  expect(screen.queryByRole('list')).not.toBeInTheDocument();

  userEvent.type(screen.getByRole('textbox'), 'my first task');
  expect(screen.getByRole('textbox')).toHaveDisplayValue('my first task');

  userEvent.click(screen.getByRole('button', { name: /add/i }));
  expect(screen.getByRole('textbox')).toHaveDisplayValue('');
  expect(screen.getByRole('list')).toBeInTheDocument();
  expect(screen.getByText('my first task')).toBeInTheDocument();

  userEvent.type(screen.getByRole('textbox'), 'one more');
  userEvent.click(screen.getByRole('button', { name: /add/i }));
  expect(screen.getByText('one more')).toBeInTheDocument();
  expect(screen.getByText('my first task')).toBeInTheDocument();

  const listScope1 = within(screen.getByRole('list'));
  userEvent.click(listScope1.getAllByRole('button')[0]);
  expect(screen.queryByText('one more')).not.toBeInTheDocument();
  expect(screen.getByText('my first task')).toBeInTheDocument();
  const listScope2 = within(screen.getByRole('list'));
  userEvent.click(listScope2.getByRole('button'));
  expect(screen.queryByText('my first task')).not.toBeInTheDocument();
  expect(screen.queryByRole('list')).not.toBeInTheDocument();
});
