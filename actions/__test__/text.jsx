// @ts-check

import { test, expect } from '@jest/globals';
import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import faker from '../src/faker.js';
import reducers from '../src/reducers/index.js';
import App from '../src/components/App.jsx';

faker.lorem.test = true;

test('Store', () => {
  const store = createStore(reducers);

  const vdom = (
    <Provider store={store}>
      <App />
    </Provider>
  );
  const { asFragment } = render(vdom);
  expect(asFragment()).toMatchSnapshot();

  const generateTasksButton = screen.getByText('Generate');
  const cleanTasksButton = screen.getByText('Clean');

  userEvent.click(generateTasksButton);
  expect(asFragment()).toMatchSnapshot();

  userEvent.click(generateTasksButton);
  expect(asFragment()).toMatchSnapshot();

  userEvent.click(cleanTasksButton);
  expect(asFragment()).toMatchSnapshot();

  userEvent.click(generateTasksButton);

  const closeButtons = screen.getAllByRole('button', { name: 'Close' });
  closeButtons.forEach(() => {
    const button = screen.getAllByRole('button', { name: 'Close' })[0];
    userEvent.click(button);
  });
  expect(asFragment()).toMatchSnapshot();
});
