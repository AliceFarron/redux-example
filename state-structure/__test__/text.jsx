// @ts-check

import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';

import reducers from '../src/reducers/index.js';
import App from '../src/components/App.jsx';

test('Store', () => {
  const store = createStore(reducers);

  const vdom = (
    <Provider store={store}>
      <App />
    </Provider>
  );

  render(vdom);

  expect(screen.getByRole('textbox')).toBeInTheDocument();
  expect(screen.getByRole('button', { name: /add/i })).toBeInTheDocument();

  userEvent.type(screen.getByRole('textbox'), 'my cool task');
  expect(screen.getByRole('textbox')).toHaveDisplayValue('my cool task');

  userEvent.click(screen.getByRole('button', { name: /add/i }));
  expect(screen.getByRole('textbox')).toHaveDisplayValue('');
  expect(screen.getByRole('link', { name: /my cool task/i })).toBeInTheDocument();

  userEvent.click(screen.getByRole('link', { name: /my cool task/i }));
  expect(screen.getByRole('link', { name: /my cool task/i })).toContainHTML('<s>my cool task</s>');
  userEvent.click(screen.getByRole('link', { name: /my cool task/i }));
  expect(screen.getByRole('link', { name: /my cool task/i })).toHaveTextContent('my cool task');
  expect(screen.getByRole('link', { name: /my cool task/i })).not.toContainHTML('<s>my cool task</s>');
});
